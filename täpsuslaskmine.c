#include <stdio.h>
#include <stdlib.h>

#define SOURCEFILE       "sharp.txt"
#define OUTPUT_FILE     "results.txt"
#define BUFF_LEN           1024
#define SHOT_POINTS      10




typedef struct 
{
    char LastName[30];
    int points[10];
    int WorstScore;
    int BestScore;
    int BullsEyeValues ;
    int totalScore;
}t_competitors;


FILE * OpenFile(const char* name,const char * type)
{
    FILE *ptr = fopen(name,type);
    
    if(ptr == NULL)
    {
        printf("File '%s' was not read access\n",name);
        exit(0);
    }
    return ptr;
}
int CountData(FILE *ptr)
{
    char buff[BUFF_LEN];
    rewind(ptr);
    int count = 0;
    while(1)
    {

        if(fgets(buff,BUFF_LEN,ptr) == NULL)
        {
            
            break;
        }
        
        count++;
    }
    
    return count;
}

int ReadData(FILE *ptr,t_competitors* competitors)
{
    char buff[BUFF_LEN];
    rewind(ptr);
    int n = 0;//counter
    
   
   while(1)
   {
       if(fgets(buff,BUFF_LEN,ptr) == NULL)
       {
           break;
       }
       if(sscanf(buff,"%s %d %d %d %d %d %d %d %d %d %d ",competitors[n].LastName,&competitors[n].points[0],&competitors[n].points[1],&competitors[n].points[2],&competitors[n].points[3],&competitors[n].points[4],&competitors[n].points[5],&competitors[n].points[6],&competitors[n].points[7],&competitors[n].points[8],&competitors[n].points[9]) == 11)
       {
           n++;//actual size if the data
       }
   }
   return n;
}

void FindWorstScore(t_competitors*competitors,int n)
{

    
    //great a nested loop
    for(int i = 0; i < n; i++)
    {
        competitors[i].WorstScore = competitors[i].points[0];//Initialize the first worst score 
        for(int j = 0; j < SHOT_POINTS;j++)
        {
            //compere the elements in array with the lowest number 
            if(competitors[i].points[j] < competitors[i].WorstScore)
            {
                competitors[i].WorstScore = competitors[i].points[j];
            }
        }
    }
   
}
void FindBestScore(t_competitors *competitors,int n)
{
    //nested loop
    for(int i = 0; i < n; i++)
    {
        competitors[i].BestScore = competitors[i].points[0];//initialize the best score 
        for(int j = 0; j < SHOT_POINTS;j++)
        {
            //compere the elements in the array to the higest number;
            if(competitors[i].points[j] > competitors[i].BestScore)
            {
                competitors[i].BestScore = competitors[i].points[j];// new higest number
            }
        }
    }
    
   
}
void FullHitCounter(t_competitors *competitors,int n)
{

    for(int i = 0 ; i < n;i++)
    {
        for(int j = 0; j < SHOT_POINTS;j++)
        {
            if(competitors[i].points[j] == SHOT_POINTS)
            {
                competitors[i].BullsEyeValues ++;
            }
        }
    }
    
   
}
void structInit(t_competitors *competitor,int n)
{
    for(int i = 0; i < n;i++)
    {
        competitor[i].BullsEyeValues = 0;
        competitor[i]. totalScore = 0;
    }
}
void PlayersTotalScore(t_competitors* competitors, int n)
{
    for(int i = 0; i < n;i++)
    {
        for(int j = 0; j < SHOT_POINTS;j++)
        {
            competitors[i].totalScore += competitors[i].points[j];
        }
    }
    
}
//Sorting data highest to lowest 
int compfnc(const void *a, const void *b){
	t_competitors *i1 = (t_competitors *)a;
	t_competitors *i2 = (t_competitors *)b;
	
		return i2->totalScore -  i1->totalScore;

}

void SaveData(FILE * f, t_competitors* competitors,int n)
{
    fprintf(f,"Name\t\tBest Score\tWorst Score\tBulls eyes(10 points) counter\tTotal Score\n");
    for(int i = 0; i < n; i++)
    {
        fprintf(f,"%-16s\t%-15d%-20d%-25d%d\n",competitors[i].LastName,competitors[i].BestScore,competitors[i].WorstScore,competitors[i].BullsEyeValues,competitors[i].totalScore);
    
    }

}

int main(void )
{
    FILE *f = OpenFile(SOURCEFILE,"r");// lugemiseks
    FILE *out = OpenFile(OUTPUT_FILE,"w");//kirjutamiseks
   
    int count = CountData(f); //read the line values  
 
    t_competitors competitors[count];

    int n = ReadData(f,competitors);
    fclose(f);//close file, as it is no longer needed
    FindWorstScore(competitors,n);//find the lowest score
    FindBestScore(competitors,n);//find the highest score
    structInit(competitors,n);//initialize struct members 
    FullHitCounter(competitors,n);//MAX point counter
    PlayersTotalScore(competitors,n);//find the total score
    qsort(competitors,n,sizeof(t_competitors),compfnc);//sort the data
    SaveData(out,competitors,n);//save the data to file called 'results.txt'
    fclose(out);//close  file
    return 0;
}
